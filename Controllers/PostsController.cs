﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hashtag_Finder.Models;
using Hashtag_Finder.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hashtag_Finder.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        public PostService PostService { get; }
        
        public PostsController(PostService postService)
        {
            this.PostService = postService;
        }

        [HttpGet]
        public IEnumerable<Post> Get()
        {
            return PostService.GetPosts();
        }

        //[HttpPatch] "[FromBody]"
        [Route("upvote")]
        [HttpGet]
        public ActionResult Get([FromQuery] int postId, [FromQuery] int userId)
        {
            PostService.AddUpVote(postId, userId);
            return Ok();
        }
    }
}