﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using Hashtag_Finder.Models;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Hosting;


namespace Hashtag_Finder.Services
{
    public class TelegramMessageService
    {
        public IWebHostEnvironment WebHostEnvironment { get; }

        public TelegramMessageService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }
        
        private string TESTDirPath
        {
            //get { return Path.Combine(WebHostEnvironment.WebRootPath, "TestData"); }
            get { return "C:\\Users\\Suiko\\source\\repos\\Hashtag Finder\\Hashtag Finder\\TestData"; }
        }


        private string TESTFilePath
        {
            //get { return Path.Combine(WebHostEnvironment.WebRootPath, "TestData", "ExampleTelegramMSG.html"); }
            get { return "C:\\Users\\Suiko\\source\\repos\\Hashtag Finder\\Hashtag Finder\\TestData\\ExampleTelegramMSG.html";  }
        }

        //public IEnumerable<TelegramMessage> GetTestData_Messages()
        public IEnumerable<Post> GetTestData_Posts()
        {
            List<TelegramMessage> master_messages = new List<TelegramMessage>();
            List<Post> master_posts = new List<Post>();
            Post currentPost = null;

            DirectoryInfo dir = new DirectoryInfo(TESTDirPath);
            FileInfo[] files = dir.GetFiles();
            List<string> fileNames = new List<string>();
            foreach (FileInfo f in files) { fileNames.Add(f.Name); }

            bool f_ChainToPost = false;
            foreach (string fileName in fileNames)
            {

                /* Telegram_Message.cs
                 * Username
                 * MessageID
                 * Content
                 */
                /* Helpful_Post.cs
                 * Associated hashtags
                 * Username
                 * Upvotes given //QUESTION: Should upvotes be given per hashtag?
                 * All Associated Messages to post
                */
                
                

                //TelegramMessage tMessage = new TelegramMessage();

                HtmlDocument myTestHTML = new HtmlDocument();
                string filePath = TESTDirPath + "/" + fileName;
                string lastUsername = null;

                myTestHTML.Load(filePath);
                if (myTestHTML.DocumentNode == null) { return null; }

                //List<HtmlNode> myNodes = myTestHTML.DocumentNode.Descendants()
                // .Select(y => y.Descendants()
                // .Where(x => x.Attributes["class"].Value == "body"))
                // .ToList();

                //I need to pull <div class="message, default"> and grab username and text

                List<HtmlNode> msgNodes = myTestHTML.DocumentNode.Descendants().Where
                    (x => (x.Name == "div" && x.Attributes["class"] != null
                    && x.Attributes["class"].Value.Contains("message default"))).ToList();
                if (msgNodes.Count != 0)
                {
                    
                    foreach(HtmlNode msgNode in msgNodes)
                    {
                        f_ChainToPost = false; //reset flag

                        HtmlNode usernameNode = msgNode.Descendants()
                            .Where(x => (x.Name == "div" && x.Attributes["class"] != null
                            && x.Attributes["class"].Value.Contains("from_name"))).FirstOrDefault();
                        HtmlNode textNode = msgNode.Descendants().Where
                            (x => (x.Name == "div" && x.Attributes["class"] != null
                            && x.Attributes["class"].Value.Contains("text"))).FirstOrDefault();
                        
                        
                        

                        /*Telegram username and text html always has 
                             * "\n" at start and "\n     " at end 
                             * so let's split the string and grab the middle node.
                             * Since it's XML escaped we need to unescape it using htmlDecode
                             */

                        //Get Username to use and un-escape the xml text
                        if (usernameNode == null)
                        {
                            f_ChainToPost = true;
                        }
                        else
                        {
                            string unescapeUsername = System.Web.HttpUtility.HtmlDecode(usernameNode.InnerText);
                            lastUsername = (unescapeUsername.Split("\n"))[1];
                        }

                        if (textNode == null)
                        {
                            continue;
                            //If textNode == null we should just continue the loop, 
                            //The textNode is sometimes empty as it's a Gif, sticker, or image message type not a "text" which I'm not handling atm
                            //WARNIGN: If we simply use continue; We MUST do this after username parsing as the user might post a sticker (null textNode), then post an imporant message (null username).
                            //So until refactoring this, keep it below the if/else un-escaping the username out of the html
                        }

                        if (lastUsername == null)
                        {
                            Console.WriteLine("TODO: Throw exception as Username and userNameNode is null!!!");
                        }

                        //Get Message ID
                        /*Telegram text html always has 
                             * "\n" at start and "\n     " at end 
                             * so let's split the string and grab the middle node.
                             */


                        
                        
                        //Get Message Content
                        string unescapeMessage = System.Web.HttpUtility.HtmlDecode(textNode.InnerText);
                        string msgContent = (unescapeMessage.Split("\n"))[1];
                        string msgOuterHtml = msgNode.OuterHtml;
                        string expr = @"message(\d+)";

                        /* From debug: msgNode.OuterHtml = "<div class=\"message default clearfix\" id=\"message2\">\n\n      <div class=\"pull_left userpic_wrap\">\n\n       <div class=\"userpic userpic5\" style=\"width: 42px; height: 42px\">\n\n        <div class=\"initials\" style=\"line-height: 42px\">\nZ(\n   ...
                         */
                        string outerHtml = msgNode.OuterHtml;
                        
                        Match m = Regex.Matches(msgOuterHtml, expr).FirstOrDefault();
                        if(m == null) { /* TODO: Handle exception */ }
                        expr = @"(\d+)";
                        Match mID = Regex.Matches(m.Value, expr).FirstOrDefault();
                        int msgID = 0;
                        Int32.TryParse(mID.Value, out msgID);


                        TelegramMessage newMsg = new TelegramMessage(msgID, lastUsername, msgContent);

                        //Arrange chain-messages as "posts" or start new chains
                        if (f_ChainToPost)
                        {
                            currentPost.AddNewMessage(newMsg);
                        }
                        else
                        {
                            currentPost = new Post(newMsg);
                            
                            //Check for hashtag and add if found
                            string str = newMsg.Content;
                            str = str.ToLower();
                            string hashtagExpr = @"(^|\B)#(?![0-9_]+\b|go_to_message)([a-zA-Z0-9_]{1,30})(\b|\r)";
                            MatchCollection hashtagMC = Regex.Matches(str, hashtagExpr);
                            foreach (Match hashtagM in hashtagMC)
                            {
                                currentPost.HashTags.Add(hashtagM.ToString());
                            }
                            master_posts.Add(currentPost);
                        }

                        master_messages.Add(newMsg);
                        

                        if(master_messages.Count() > 9) //TODO: Remove as this is for testing
                        {
                            break;
                        }
                        //TODO: Add #hashtags to Post.cs
                    }
                }
                ////If I pull a <div class="message, default, joined"> I need to use last username
                //List<HtmlNode> msgtest = myTestHTML.DocumentNode.Descendants().Where
                //    (x => (x.Name == "div" && x.Attributes["class"] != null
                //    && x.Attributes["class"].Value.Contains("from_name"))).ToList();

                //List<HtmlNode> msgName = myTestHTML.DocumentNode.Descendants().Where
                //    (x => (x.Name == "div" && x.Attributes["class"] != null
                //    && x.Attributes["class"].Value.Contains("from_name"))).ToList();


                //List<HtmlNode> msgText = myTestHTML.DocumentNode.Descendants().Where
                //    (x => (x.Name == "div" && x.Attributes["class"] != null
                //    && x.Attributes["class"].Value.Contains("text"))).ToList();

                //Clean up message text and just get the raw list of hashtags used

                
                /*
                * I have a file
                * Make a TelegramMessage.cs object
                    * I need to pull <div class="message,default"> but it can not have "joined" - this is the start of a message chain
                        * From within it has a <div class="body">
                            * From within body div it has a <div class="from_name"> which is :UserName:
                                * Grab Username Text and push it into TelegramMessage.cs Object
                            * From within body div it has a <div class="text"> which is the acutal :Message:
                                * Grab Text and push it into TelegramMessage.cs Object
                    * I then need to pull any future divs with <div class="message, default, joined"> as these are chained messages
                        * From within it has a <div class="body">
                            * From within this body div it has a <div class="text"> which is the acutal :Message: but no username
                */

            }

            //return master_posts;
            return FirstTen(master_posts);
        }
        private List<Post> FirstTen(List<Post> posts)
        {
            List<Post> r = new List<Post>();
            for(int i = 0; i < 10; i++)
            {
                r.Add(posts[i]);
            }
            return r;
        }
               
        public IEnumerable<HashTag> GetTestData_HashtagList()
        {
            //TODO: Refactor to handle multiple files better
            List<HashTag> master_hashtags = new List<HashTag>();
            DirectoryInfo dir = new DirectoryInfo(TESTDirPath);
            FileInfo[] files = dir.GetFiles();
            List<string> fileNames = new List<string>();
            
            foreach(FileInfo f in files) { fileNames.Add(f.Name); }
            

            foreach( string fileName in fileNames)
            {
                //Grab every single hashtag out of the html file messages
                List<string> raw_hashtags = GetRawHashTagsFromFile(TESTDirPath + "/" + fileName);
                if (raw_hashtags == null) { continue; }

                //Let's merge the Raw_hashtags list into a master hashtag list as we want to have the count total.
                foreach (string item in raw_hashtags)
                {
                    /*map items into List<HashTag> and apply counting to it*/
                HashTag obj = null;
                    if (master_hashtags.Count > 0) {
                        //obj = hashtags.Where(x => x.Name == item).First();
                        obj = master_hashtags.FirstOrDefault(x => x.Name == item);
                    }
                    if (obj != null)
                    {
                        obj.Count += 1;
                    }
                    else
                    {
                        master_hashtags.Add(new HashTag(item));
                    }
                }
            } //End Foreach fileName


            return SortMyList(master_hashtags);
        }
        private List<HashTag> SortMyList(List<HashTag> hashtag_list)
        {
            return hashtag_list.OrderByDescending(x => x.Count).ToList(); ;
        }
        private List<string> GetRawHashTagsFromFile(string filePath)
        {
            HtmlDocument myTestHTML = new HtmlDocument();
            List<string> raw_hashtags = new List<string>();

            myTestHTML.Load(filePath);

            if (myTestHTML.DocumentNode == null){ return null; }
            
            //List<HtmlNode> msgName = myTestHTML.DocumentNode.Descendants().Where
            //    (x => (x.Name == "div" && x.Attributes["class"] != null
            //    && x.Attributes["class"].Value.Contains("from_name"))).ToList();


            List<HtmlNode> msgText = myTestHTML.DocumentNode.Descendants().Where
                (x => (x.Name == "div" && x.Attributes["class"] != null
                && x.Attributes["class"].Value.Contains("text"))).ToList();

            //Clean up message text and just get the raw list of hashtags used

            foreach (HtmlNode item in msgText)
            {
                string str = item.InnerHtml.ToString();
                str = str.ToLower();
                string expr = @"(^|\B)#(?![0-9_]+\b|go_to_message)([a-zA-Z0-9_]{1,30})(\b|\r)";
                MatchCollection mc = Regex.Matches(str, expr);
                foreach (Match m in mc)
                {
                    raw_hashtags.Add(m.ToString());
                }
            }

          
            return raw_hashtags;
        }


    }
}
