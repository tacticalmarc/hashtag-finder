﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using Hashtag_Finder.Models;
using Microsoft.AspNetCore.Hosting;


namespace Hashtag_Finder.Services
{
    public class PostService
    {
        private IWebHostEnvironment WebHostEnvironment { get; }

        private TelegramMessageService TestDatabase { get; set;  } //TODO: Remove test Database Service and add database calls

        public PostService(IWebHostEnvironment webHostEnvironment, TelegramMessageService telegramMessageService)
        {
            this.WebHostEnvironment = webHostEnvironment;
            this.TestDatabase = telegramMessageService;
        }

        public IEnumerable<Post> GetPosts()
        {
            return TestDatabase.GetTestData_Posts(); //TODO: Remove test Database and add actual database calls
        }

        public Post AddUpVote(int postId, int userId)
        {
            bool success = false;
            var posts = GetPosts();
            var query = posts.FirstOrDefault(x => x.Id == postId);
            if(query == null){
                //ERROR: No post with %postId% found
                return null;
            }
            query.AddUpVote(userId);
            return query as Post;
        }
    }
}
