﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hashtag_Finder.Models;
using Hashtag_Finder.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Hashtag_Finder.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public TelegramMessageService TelegramMessageService { get; set; }
        //public IEnumerable<Post> TelegramPosts { get; set; }
        public IEnumerable<HashTag> HashTags { get; set; }
        public string TestMessage { get; set; }
        public IndexModel(ILogger<IndexModel> logger,
            TelegramMessageService telegramMessageService)
        {
            _logger = logger;
            TelegramMessageService = telegramMessageService;
            TestMessage = "";
        }

        public void OnGet()
        {
            //TelegramPosts = TelegramMessageService.GetTestData_Posts();
            HashTags = TelegramMessageService.GetTestData_HashtagList();

        }
    }
}
