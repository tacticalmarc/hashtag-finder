﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hashtag_Finder.Models;
using Hashtag_Finder.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Hashtag_Finder
{
    public class THashtagsModel : PageModel
    {
        private readonly ILogger<THashtagsModel> _logger;
        public TelegramMessageService TelegramMessageService { get; set; }
        public IEnumerable<Post> TelegramPosts { get; set; }
        public IEnumerable<Post> GetFirst50Posts
        {
            get
            {
                return TelegramPosts.Take<Post>(50);
            }
        }

        public THashtagsModel(ILogger<THashtagsModel> logger,
            TelegramMessageService telegramMessageService)
        {
            _logger = logger;
            TelegramMessageService = telegramMessageService;
        }

        public void OnGet()
        {
            IEnumerable<Post> msgs = TelegramMessageService.GetTestData_Posts();
            //IEnumerable<HashTag> hashtags = TelegramMessageService.GetTestData_HashtagList();
            List<Post> marketingMsgs = new List<Post>();
            foreach( Post m in msgs)
            {
                if( m.HashTags.Count > 0)
                {
                    if (m.HashTags.Contains("#marketing"))
                    {
                        marketingMsgs.Add(m);
                    }
                }
            }
            //IEnumerable<Post> marketingMsgs = msgs.Where(
            //    m => m.HashTags.Count > 0
            //    );
            //Use #marketing as an example page
            
            TelegramPosts = marketingMsgs;
        }
    }
}