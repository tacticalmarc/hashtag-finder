﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hashtag_Finder.Models;
using Hashtag_Finder.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Hashtag_Finder.Pages
{
    public class TPostsModel : PageModel
    {
        private readonly ILogger<TPostsModel> _logger;
        public TelegramMessageService TelegramMessageService { get; set; }
        public IEnumerable<Post> TelegramPosts { get; set; }
        public IEnumerable<Post> GetFirst50Posts { 
            get
            {
                return TelegramPosts.Take<Post>(50);
            } 
        }

        public TPostsModel(ILogger<TPostsModel> logger,
            TelegramMessageService telegramMessageService)
        {
            _logger = logger;
            TelegramMessageService = telegramMessageService;
        }

        public void OnGet()
        {
            TelegramPosts = TelegramMessageService.GetTestData_Posts();

        }
    }
}