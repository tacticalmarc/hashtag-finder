﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hashtag_Finder.Models
{
    public class Post
    {
        public int Id { get; private set; }
        public string UserName { get; set; }

        private int[] Upvotes { get; set; }

        public int UpvotesCount
        {
            get
            {
                if (Upvotes == null) { return 0; }
                else { return Upvotes.Count(); }
            }
        }

        public string MsgLink
        {
            get
            {
                string link = "https://t.me/c/1318997939/" + Messages.First().MessageID;
                return link;
            }
        }
        private string _content = null;
        
        
        public string Content {
            get
            {
                //TODO: @Html.Raw(Html.Encode(post.Content).Replace("\n", "<br />")) isn't functioning as I expected
                if (_content == null) { 
                    if(Messages == null) { Console.WriteLine("TODO: Throw Exception"); }
                    string r = "";
                    foreach (TelegramMessage m in Messages)
                    {
                        //TODO: Re-do this property which is mostly being used for debugging, or just utlize Messages for front-end
                        r = String.Concat(r, m.Content);
                        r = String.Concat(r, " \n ");
                    }
                    _content = r;
                }
                return _content;
            } 
        }

        public List<string> HashTags { get; set; }

        private List<TelegramMessage> _messages = new List<TelegramMessage>();
        public List<TelegramMessage> Messages { get { return _messages; } }

        public Post(TelegramMessage firstMsgInChain)
        {
            HashTags = new List<string>();
            UserName = firstMsgInChain.UserName;
            AddNewMessage(firstMsgInChain);
        }

        //TODO: Search and cache all hashtags in every given post

        public void AddNewMessage(TelegramMessage msg)
        {
            _content = null; //clear cached content as we have new content to parse in //TODO: refactor to just extend _content
            Messages.Add(msg);
        }

        public bool AddUpVote(int userId)
        {
            bool success = false;

            if (Upvotes == null){
                Upvotes = new int[] { userId };
            }
            else{
                List<int> upvotes = Upvotes.ToList();
                if (upvotes.Contains(userId)){
                    //ERROR: User Already Upvoted, view shouldn't be exposing this function
                }
                else{
                    upvotes.Add(userId);
                    Upvotes = upvotes.ToArray();
                    success = true;
                }
            }
            return success;
        }
    }
}
