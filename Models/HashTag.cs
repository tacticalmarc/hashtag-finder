﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hashtag_Finder.Models
{
    public class HashTag
    {
        public string Name { get; set; }
        public int Count { get; set; }

        public HashTag(string name)
        {
            Name = name;
            Count = 1;
        }
    }
}
