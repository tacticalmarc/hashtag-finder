﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hashtag_Finder.Models
{
    public class TelegramMessage
    {
        public int MessageID { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }

        public TelegramMessage(int msgID, string msgUserName, string msgContent)
        {
            MessageID = msgID;
            UserName = msgUserName;
            Content = msgContent;
        }
    }
}